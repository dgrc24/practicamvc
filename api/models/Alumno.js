/**
 * Alumno.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    NC: {
      type: "string",
    },
    nombre: {
      type: "string",
    },
    apellido: {
      type: "string",
    },
    semestre: {
      type: "string",
    },
    grupo: {
      type: "string",
    },
    domicilio: {
      type: "string",
    },
    fechaNac: {
      type: "string",
    },
    telefono: {
      type: "string",
    },
  },
  datastores: "default",
};
