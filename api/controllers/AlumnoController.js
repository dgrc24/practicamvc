/**
 * AlumnoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  vermostrar: function (req, res) {
    res.view("mostrar");
  },
  home: function (req, res) {
    res.view("\\pages\\homepage");
  },
  mostrar: function () {
    Alumno.find().exec(function (err, alumno) {
      if (err) {
        res.send(500, { error: "Database error" });
      }
      res.view("mostrar", { alumno: alumno });
    });
  },
};
